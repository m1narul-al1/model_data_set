import 'package:flutter/material.dart';
import 'package:flutter_app_model/model/FamilyMember.dart';
import 'package:flutter_app_model/model/PersonalDetails.dart';

import '../main.dart';

class Dashboard extends StatefulWidget {
  PersonalDetails personalDetails;
  final familyMemberResponce;
  final String sId;

  Dashboard({Key key, this.sId, this.familyMemberResponce}) : super(key: key);

  DashboardState createState() => DashboardState(
        sId: sId,
        familyMemberResponce: familyMemberResponce,
      );
}

class DashboardState extends State<Dashboard> {
  PersonalDetails personalDetails;
  var familyMemberResponce;
  String sId;

  DashboardState({@required this.sId, @required this.familyMemberResponce});

  @override
  void initState() {
    super.initState();
    if (personalDetails == null) {
      _fatchPersonalDetails();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(ModalRoute.of(context).debugLabel);
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
        ),
        body: SafeArea(
            child: SizedBox.expand(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildSelf(),
            ],
          ),
        )));
  }

  Widget _buildSelf() {
    print(personalDetails);
    if (personalDetails?.faildReson == 'Invalid SecurityID' ||
            familyMemberResponce is FamilyMemberResponce
        ? familyMemberResponce.faildReson == 'Invalid SecurityID'
        : false) {
      return Expanded(
        child: Container(
          child: AlertDialog(
            title: Text('Invalid Security ID'),
            content: Text('This Security ID is no longer available'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  print(Navigator.defaultRouteName);
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => MyHomePage()),
                      (Route<dynamic> route) => false);
                },
              ),
            ],
          ),
        ),
      );
    }
    return Container(
      width: 80.0,
      height: 80.0,
      margin: EdgeInsets.all(10.0),
      child: personalDetails == null
          ? CircularProgressIndicator()
          : Text(personalDetails.personal.firstName),
      /*decoration: new BoxDecoration(
            color: Colors.green,
            shape: BoxShape.circle,
            image: new DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage(personalDetails.personal.profilePicture)
            )
        )*/
    );
  }

  Widget _buildChild() {
    if (familyMemberResponce is FamilyMemberResponce) {
      if (familyMemberResponce.faildReson == 'Invalid SecurityID') {
        return Expanded(
          child: Container(
            child: AlertDialog(
              title: Text('Invalid Security ID'),
              content: Text('This Security ID is no longer available'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    }
    if (familyMemberResponce is List) {
      if (familyMemberResponce.length > 1) {
        return Row(
          children: <Widget>[
            _buildFamilyMember(familyMemberResponce[0].memberName),
            _buildFamilyMember(familyMemberResponce[1].memberName),
          ],
        );
      } else if (familyMemberResponce.length == 1) {
        return Row(
          children: <Widget>[
            _buildFamilyMember(familyMemberResponce[0].memberName),
            //_buildAddMember(),
          ],
        );
      }
    }
    //return _buildAddMember();
  }

  Widget _buildFamilyMember(String url) {
    return Row(
      children: <Widget>[
        Container(
          width: 80.0,
          height: 80.0,
          margin: EdgeInsets.all(10.0),
          child: Text(url),
          /*decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.green,
                image: new DecorationImage(
                    fit: BoxFit.fill, image: new NetworkImage(url)))*/
        ),
      ],
    );
  }

  Widget _buildAddMember() {
    return Container(
        width: 80.0,
        height: 80.0,
        margin: EdgeInsets.all(10.0),
        child: IconButton(
          onPressed: () {},
          icon: Icon(Icons.add),
        ),
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.greenAccent,
        ));
  }

  void _fatchPersonalDetails() async {
    var personalDetails = await fetchPostPersonalDetails('ld9JwEhSG9dbeRlkTesg');
    setState(() {
      this.personalDetails = personalDetails;
    });
  }
}
