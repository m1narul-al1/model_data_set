import 'package:http/http.dart' as http;
import 'dart:convert';
class Person {
  final int userInfoStatus;
  final String securityID;
  final String status;
  final String faildReson;

  Person({this.userInfoStatus, this.securityID, this.status, this.faildReson});

  factory Person.fromJson(Map<String, dynamic> json) {
    return Person(
      userInfoStatus: json['UserInfoStatus'],
      securityID: json['SecurityID'],
      status: json['Status'],
      faildReson: json['body'],
    );
  }

}
Future<Person> fetchPost(String mobile,String password,String userType) async {
  print(mobile);
  print(password);
  var uri = Uri.http("cure-staging-api.azurewebsites.net", Uri.encodeFull("/api/account/login"), { "Mobile" : mobile,"Password" : password, "UserType" : userType});
  print(uri);
  final response =
  await http.post(uri);
  print(response.statusCode);
  print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    return Person.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }

}