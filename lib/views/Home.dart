import 'package:flutter/material.dart';
import 'package:flutter_app_model/model/FamilyMember.dart';
import 'package:flutter_app_model/model/PersonalDetails.dart';
import 'package:flutter_app_model/views/Dashboard.dart';
import 'package:passcode/passcode.dart';
import 'PassCode.dart';
import 'package:path/path.dart';

class Rout extends StatefulWidget{
  final String sId;
  Rout({Key key, @required this.sId}) : super(key: key);
  RoutState createState() => RoutState(sId: sId);
}
class RoutState extends State<Rout> with WidgetsBindingObserver{
  AppLifecycleState _state;
  final String sId;
  RoutState({Key key, @required this.sId});
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _state = state;
    });
  }
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }
  @override
  Widget build(BuildContext context) {
    print(_state);
    if(_state != null){

      return PasscodeTextField(
        onTextChanged: (passcode) {
          print(passcode);
          if(passcode == '1234'){
            print(ModalRoute.of(context).settings.name);
            print(ModalRoute.of(context).currentResult);
            Navigator.of(context, rootNavigator: true).pop(this);
          }
        },
        totalCharacters: 4,
        borderColor: Colors.black,
        passcodeType: PasscodeType.number,
      );

    }
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{},
      home: Home(sId: sId,),
    );
  }

}
class Home extends StatefulWidget  {
  final String sId;
  Home({Key key, @required this.sId}) : super(key: key);
  @override
  HomeState createState() => new HomeState(key: key,sId: sId);
}
class HomeState extends State<Home> {
  final String sId;
  PersonalDetails personalDetails;
  var familyMemberResponce;

  HomeState({Key key, @required this.sId});

  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    print(ModalRoute.of(context).settings.name);
    print(ModalRoute.of(context).debugLabel);
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text('SecurityID :' + sId),
                ),
              ],
            ),
            RaisedButton(onPressed: (){_dashboard(context);},child: Text('Dashboard'),color: Colors.lightBlueAccent,),
          ],
        ));
  }


  void _dashboard(BuildContext context) async {
    _handleSignIn().whenComplete(() => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Dashboard(sId: sId,
                familyMemberResponce: familyMemberResponce))));
  }

  _handleSignIn() async {
    //print(personalDetails);
    familyMemberResponce = await fetchPostFamilyMember(sId);
  }

}

