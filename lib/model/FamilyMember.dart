import 'package:http/http.dart' as http;
import 'dart:convert';
class FamilyMemberResponce {
  final String status;
  final String faildReson;
  final String code;

  FamilyMemberResponce({this.status, this.faildReson, this.code});

  factory FamilyMemberResponce.fromJson(Map<String, dynamic> json) {
    return FamilyMemberResponce(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }

}
class FamilyMember{

  String userID,
  memberName,
  relationName,
  gender,
  dOB,
  profilePicture,
  phoneNo,
  email,
  createdDate;

  FamilyMember(this.userID, this.memberName, this.relationName, this.gender,
      this.dOB, this.profilePicture, this.phoneNo, this.email,
      this.createdDate);


}
Future<dynamic> fetchPostFamilyMember(String securityID) async {
  var body = json.encode({"SecurityID": securityID});
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/Patient/GetFamilyDetails"));
  print(uri);
  final response = await http.post(uri,
      headers: {"Content-Type": "application/json"}, body: body);
  print(response.body);

  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if (json.decode(response.body) is List) {
      List responseJson = json.decode(response.body);
      List<FamilyMember> userList = createUserList(responseJson);
      print(userList);
      return userList;
    }
    if (json.decode(response.body) is Map) {
      return FamilyMemberResponce.fromJson(json.decode(response.body));
    }
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<FamilyMember> createUserList(List data) {
  List<FamilyMember> list = new List();
  for (int i = 0; i < data.length; i++) {
    String userID = data[i]["UserID"];
    String memberName = data[i]["memberName"];
    String relationName = data[i]["RelationName"];
    String gender = data[i]["Gender"];
    String dOB = data[i]["DOB"];
    String profilePicture = data[i]["ProfilePicture"];
    String phoneNo = data[i]["PhoneNo"];
    String email = data[i]["Email"];
    String createdDate = data[i]["CreatedDate"];
    FamilyMember fm = new FamilyMember(userID, memberName, relationName, gender,
        dOB, profilePicture, phoneNo, email, createdDate);
    list.add(fm);
  }
  return list;
}